# zsh-solar

It basically tells you if it's day or night.

I may add further functionality when I get bored.

## How to install

Assuming you're using oh-my-zsh (why wouldn't you?)

1. Download the code to your custom plugins directory

    ```zsh
    git clone git@gitlab.com:danielpatrick/zsh-solar.git $ZSH_CUSTOM/plugins/zsh-solar
    ```

2. Add the plugin to your .zshrc

    ```zsh
    plugins=(git zsh-solar)
    ```

3. Use the function(s) provided as desired, eg

    ```zsh
    prompt_context() {
      # This example is for London but to get customised
      # results use your own latitude and longitude
      if (is_daytime 51.50853 -0.12574); then
        emoji="🌤️"
      else
        emoji="🌙"
      fi
      prompt_segment black default $emoji
    }
    ```

If you use the agnoster theme, the above will replace the user@hostname with
one of the above emojis depending on the time.

## Function(s) provided

### `is_daytime`

#### Arguments

- latitude (float) - your location's latitude
- longitude (float) - your location's longitude

#### Return value

- 0 if it is currently daytime in the given location and 1 otherwise

## Caveats and credits

The calculations are a bit off so I wouldn't recommend using them for anything
serious. I probably made some mistakes while porting them [from C](https://blog.sleeplessbeastie.eu/2013/05/21/how-to-determine-the-sunrise-and-sunset-times/).

Comparing to popular online tools, they appear to be accurate within
approximately one minute but I'd still like to improve them at some point.
