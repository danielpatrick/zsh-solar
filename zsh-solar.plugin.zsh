# Version 0.1.1

zmodload zsh/mathfunc

_solar_PI=3.141592654
_solar_SUNRISE_ANGLE=-0.833

# Args: year, month, day
_solar_get_julian_day() {
  year=$1
  month=$2
  day=$3

  a=$(( (14 - $month) / 12 ))
  y=$(( $year + 4800 - $a ))
  m=$(( $month + 12 * $a - 3 ))

  echo $(( $day + (153 * $m + 2) / 5 + 365 * $y + $y / 4 - $y / 100 + $y / 400 - 32045 ))
}

_solar_julian_day_number() {
  year=`date -u +"%Y"`
  month=`date -u +"%m"`
  day=`date -u +"%d"`
  hour=`date -u +"%H"`
  minute=`date -u +"%M"`
  second=`date -u +"%S"`

  jdn=$(_solar_get_julian_day $year $month $day)
  echo $(( $jdn + ( $hour / 24.0 + $minute / (24.0 * 60.0) + $second / (24.0 * 3600.0) ) - 0.5 ))
}

# Args: dividend, divisor
_solar_mod() {
  echo $(( ($1 % $2 + $2) % $2 ))
}

# Args: angle_in_degrees
_solar_to_rad() {
  echo $(( $_solar_PI * $1 / 180 ))
}

# Args: angle_in_radians
_solar_to_deg() {
 echo $(( 180 * $1 / $_solar_PI ))
}

# Args: angle_in_degrees
_solar_full_circle() {
  echo $(( $1 - int($1 / 360) * 360 ))
}

# Args: julian_day_number
_solar_earth_mean_anomaly() {
  echo $( _solar_full_circle $(( 357.5291 + 0.98560028 * ($1 - 2451545) )) )
}

# Args: mean_anomaly
_solar_earth_equation_of_centre() {
  echo $((
    1.9148 * sin(`_solar_to_rad $1`) +
    0.0200 * sin(2 * `_solar_to_rad $1`) +
    0.0003 * sin(3 * `_solar_to_rad $1`)
  ))
}

# Args: julian_day_number
_solar_earth_true_anomaly() {
  mn_anomaly="$(_solar_earth_mean_anomaly $1)"
  eq_centre="$(_solar_earth_equation_of_centre $mn_anomaly)"
  echo $(( $mn_anomaly + $eq_centre ))
}

# Args: true_anomaly
_solar_ecliptic_longitude() {
  echo $( _solar_full_circle $(( $1 + 102.9372 + 180 )) )
}

# Args: julian_day_number, longitude
_solar_earth_sideral_time() {
  echo $( _solar_full_circle $(( 280.1600 + 360.9856235 * ($1 - 2451545) + $2 )) )
}

# Args: ecliptic_longitude
_solar_earth_right_ascension() {
  result=$((
    atan(
      sin(`_solar_to_rad $1`) * cos(`_solar_to_rad 23.45`),
      cos(`_solar_to_rad $1`)
    )
  ))
  result=$( _solar_to_deg $result )
  echo $( _solar_mod $result 360 )
}

# Args: sideral_time, right_ascension
_solar_earth_hour_angle() {
  echo $(( $1 - $2 ))
}

# Args: hour_angle, declination, latitude
_solar_earth_altitude() {
  result=$((
    asin(
      sin(`_solar_to_rad $3`) * sin(`_solar_to_rad $2`) +
      cos(`_solar_to_rad $3`) * cos(`_solar_to_rad $2`) * cos(`_solar_to_rad $1`)
    )
  ))
  echo $( _solar_full_circle $( _solar_to_deg $result ) )
}

# Args: ecliptic_longitude
_solar_earth_declination() {
  result=$(( asin(sin(`_solar_to_rad $1`) * sin(`_solar_to_rad 23.45`)) ))
  echo $( _solar_to_deg $result )
}

# Args: latitude, longitude
function is_daytime() {
  latitude=$1
  longitude=$2

  jdn=`_solar_julian_day_number`

  true_anomaly=$( _solar_earth_true_anomaly $jdn )
  ecliptic_longitude=$( _solar_ecliptic_longitude $true_anomaly )
  declination=$( _solar_earth_declination $ecliptic_longitude )
  sideral_time=$( _solar_earth_sideral_time $jdn $longitude )
  ra=$( _solar_earth_right_ascension $ecliptic_longitude )
  hour_angle=$( _solar_earth_hour_angle $sideral_time $ra )
  altitude=$( _solar_earth_altitude $hour_angle $declination $latitude )

  [[ $altitude -gt $_solar_SUNRISE_ANGLE ]] && return 0 || return 1
}
